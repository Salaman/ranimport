﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
namespace RANImport
{
    public class Settings
    {
        MainForm form;

        public Settings (MainForm form)
        {
            this.form = form;
        }
        public void Save()
        {
            Properties.Settings.Default.XmlPath = (form.Controls.Find("_XmlPath", false).FirstOrDefault() as TextBox).Text;
            Properties.Settings.Default.Server = (form.Controls.Find("_Server", false).FirstOrDefault() as ComboBox).Text;
            Properties.Settings.Default.Login = (form.Controls.Find("_Login", false).FirstOrDefault() as TextBox).Text;
            Properties.Settings.Default.Pass = (form.Controls.Find("_Pass", false).FirstOrDefault() as TextBox).Text;
            Properties.Settings.Default.Save();
        }

        public void Load()
        {
            (form.Controls.Find("_XmlPath", false).FirstOrDefault() as TextBox).Text = Properties.Settings.Default.XmlPath;
            (form.Controls.Find("_Server", false).FirstOrDefault() as ComboBox).Text = Properties.Settings.Default.Server;
            (form.Controls.Find("_Login", false).FirstOrDefault() as TextBox).Text = Properties.Settings.Default.Login;
            (form.Controls.Find("_Pass", false).FirstOrDefault() as TextBox).Text = Properties.Settings.Default.Pass;
        }
    }
}
