﻿namespace RANImport
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this._XmlPath = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this._Login = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this._Pass = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this._Log = new System.Windows.Forms.RichTextBox();
            this._Import = new System.Windows.Forms.Button();
            this._Exit = new System.Windows.Forms.Button();
            this._Server = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // _XmlPath
            // 
            this._XmlPath.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this._XmlPath.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._XmlPath.Cursor = System.Windows.Forms.Cursors.Default;
            this._XmlPath.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Bold);
            this._XmlPath.ForeColor = System.Drawing.SystemColors.Control;
            this._XmlPath.Location = new System.Drawing.Point(164, 44);
            this._XmlPath.Name = "_XmlPath";
            this._XmlPath.Size = new System.Drawing.Size(856, 33);
            this._XmlPath.TabIndex = 0;
            this._XmlPath.Click += new System.EventHandler(this._XmlPath_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.SystemColors.Control;
            this.label1.Location = new System.Drawing.Point(12, 46);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(146, 25);
            this.label1.TabIndex = 1;
            this.label1.Text = "Xml-файл: ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.SystemColors.Control;
            this.label2.Location = new System.Drawing.Point(51, 102);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 25);
            this.label2.TabIndex = 2;
            this.label2.Text = "Cервер:";
            // 
            // _Login
            // 
            this._Login.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this._Login.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Login.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Bold);
            this._Login.ForeColor = System.Drawing.SystemColors.Control;
            this._Login.Location = new System.Drawing.Point(164, 138);
            this._Login.Name = "_Login";
            this._Login.Size = new System.Drawing.Size(856, 33);
            this._Login.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.SystemColors.Control;
            this.label3.Location = new System.Drawing.Point(65, 141);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(93, 25);
            this.label3.TabIndex = 4;
            this.label3.Text = "Логин:";
            // 
            // _Pass
            // 
            this._Pass.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this._Pass.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Pass.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Bold);
            this._Pass.ForeColor = System.Drawing.SystemColors.Control;
            this._Pass.Location = new System.Drawing.Point(164, 177);
            this._Pass.Name = "_Pass";
            this._Pass.Size = new System.Drawing.Size(856, 33);
            this._Pass.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Bold);
            this.label4.ForeColor = System.Drawing.SystemColors.Control;
            this.label4.Location = new System.Drawing.Point(48, 180);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(110, 25);
            this.label4.TabIndex = 6;
            this.label4.Text = "Пароль:";
            // 
            // _Log
            // 
            this._Log.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this._Log.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this._Log.Cursor = System.Windows.Forms.Cursors.Default;
            this._Log.DetectUrls = false;
            this._Log.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._Log.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold);
            this._Log.ForeColor = System.Drawing.SystemColors.Control;
            this._Log.Location = new System.Drawing.Point(0, 352);
            this._Log.Name = "_Log";
            this._Log.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this._Log.Size = new System.Drawing.Size(1033, 530);
            this._Log.TabIndex = 8;
            this._Log.Text = "";
            // 
            // _Import
            // 
            this._Import.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this._Import.FlatAppearance.BorderSize = 0;
            this._Import.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(110)))), ((int)(((byte)(110)))));
            this._Import.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(120)))), ((int)(((byte)(120)))));
            this._Import.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._Import.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Bold);
            this._Import.ForeColor = System.Drawing.SystemColors.Control;
            this._Import.Location = new System.Drawing.Point(12, 227);
            this._Import.Name = "_Import";
            this._Import.Size = new System.Drawing.Size(1008, 113);
            this._Import.TabIndex = 9;
            this._Import.TabStop = false;
            this._Import.Text = "Начать импорт";
            this._Import.UseVisualStyleBackColor = false;
            this._Import.Click += new System.EventHandler(this._Import_Click);
            // 
            // _Exit
            // 
            this._Exit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._Exit.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this._Exit.FlatAppearance.BorderSize = 0;
            this._Exit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(110)))), ((int)(((byte)(110)))));
            this._Exit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(120)))), ((int)(((byte)(120)))));
            this._Exit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._Exit.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._Exit.ForeColor = System.Drawing.SystemColors.Control;
            this._Exit.Location = new System.Drawing.Point(969, 0);
            this._Exit.Name = "_Exit";
            this._Exit.Size = new System.Drawing.Size(64, 24);
            this._Exit.TabIndex = 14;
            this._Exit.TabStop = false;
            this._Exit.Text = "X";
            this._Exit.UseVisualStyleBackColor = false;
            // 
            // _Server
            // 
            this._Server.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this._Server.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this._Server.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._Server.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._Server.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._Server.ForeColor = System.Drawing.SystemColors.Control;
            this._Server.FormattingEnabled = true;
            this._Server.Location = new System.Drawing.Point(164, 99);
            this._Server.Name = "_Server";
            this._Server.Size = new System.Drawing.Size(856, 34);
            this._Server.Sorted = true;
            this._Server.TabIndex = 15;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold);
            this.label5.ForeColor = System.Drawing.SystemColors.Control;
            this.label5.Location = new System.Drawing.Point(9, 5);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(80, 14);
            this.label5.TabIndex = 16;
            this.label5.Text = "RANImport";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.ClientSize = new System.Drawing.Size(1033, 882);
            this.Controls.Add(this.label5);
            this.Controls.Add(this._Server);
            this.Controls.Add(this._Exit);
            this.Controls.Add(this._Import);
            this.Controls.Add(this._Log);
            this.Controls.Add(this._Pass);
            this.Controls.Add(this.label4);
            this.Controls.Add(this._Login);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this._XmlPath);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MainForm";
            this.Text = "RANImport";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox _XmlPath;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox _Login;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox _Pass;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.RichTextBox _Log;
        private System.Windows.Forms.Button _Import;
        private System.Windows.Forms.Button _Exit;
        private System.Windows.Forms.ComboBox _Server;
        private System.Windows.Forms.Label label5;
    }
}

