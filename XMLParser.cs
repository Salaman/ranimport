﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Windows.Forms;
using System.IO;
using System.Xml.Linq;

namespace RANImport
{
    public class XMLParser
    {
        public DataSet Database;

        public DataTable Archive;
        public DataTable Fund;

        public XMLParser()
        {

        }
        public void Parse(string filepath)
        {
            XDocument doc = XDocument.Load(filepath);
            Database = new DataSet("Database");


            //Архив
            Log.Append("Чтение архива из xml-документа");
            Archive = new DataTable("Archive");
            XElement element = doc.Root.Element("ARCHIVE");
            List<XElement> archiveElements = element.Descendants().ToList();
            //фильтрация нод для архива
            archiveElements.RemoveAll((n) => n.Parent.Name == "FUND" || n.Parent.Name == "FUNDS");
            archiveElements.Remove(archiveElements.Find((n) => n.Name == "FUNDS"));
            //Создание столбцов в таблице Archives
            DataRow drAchive = Archive.Rows.Add();
            foreach (XElement n in archiveElements)
            {
                string colName = n.Name.ToString();
                if (!Archive.Columns.Contains(colName))
                    Archive.Columns.Add(colName);
                drAchive[colName] = n.Value;
            }
            Log.Done(Archive.Rows.Count > 0);


            //Фонды
            Log.Append("Чтение фондов из xml-документа");
            Fund = new DataTable("Fund");
            element = doc.Descendants("FUND").FirstOrDefault();
            do
            {
                List<XElement> fundElements = element.Descendants().ToList();

                DataRow drFund = Fund.Rows.Add();
                foreach (XElement n in fundElements)
                {
                    string colName = n.Name.ToString();
                    if (!Fund.Columns.Contains(colName))
                        Fund.Columns.Add(colName);
                    drFund[colName] = n.Value;
                }
                element = (XElement)element.NextNode;
            } while (element != null);
            Log.Done(Fund.Rows.Count > 0);


            //Описи

            //Ед. хр.

            Database.Tables.Add(Archive);
            Database.Tables.Add(Fund);
        }
    }
}
