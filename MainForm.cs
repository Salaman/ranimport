﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
namespace RANImport
{
    public partial class MainForm : Form
    {
        SQLClient sql;
        Settings settings;
        XMLParser xml;

        public MainForm()
        {
            #region Инициализация графики
            InitializeComponent();
            #endregion

            settings = new Settings(this);
            sql = new SQLClient();
            xml = new XMLParser();
            Log.TextBox = _Log;

            #region Сброс фокуса
            Load += (s, a) => ActiveControl = null;
            _XmlPath.MouseDown += (s, a) => ActiveControl = null;
            _Import.Click += (s, a) => ActiveControl = null;
            MouseDown += (s, a) => ActiveControl = null;
            label1.MouseDown += (s, a) => ActiveControl = null;
            label2.MouseDown += (s, a) => ActiveControl = null;
            label3.MouseDown += (s, a) => ActiveControl = null;
            label4.MouseDown += (s, a) => ActiveControl = null;
            label5.MouseDown += (s, a) => ActiveControl = null;
            _Server.SelectedIndexChanged += (s, a) => ActiveControl = null;
            _Log.Enter += (s, a) => ActiveControl = null;
            #endregion Сброс фокуса
            #region Перемещение окна за любой участок
            MouseDown += OnMouseDown; MouseMove += OnMouseMove;
            foreach (Control c in Controls)
            {
                if (!(c is ComboBox) && c.Name != "_XmlPath" && c.Name != "_Login" && c.Name != "_Pass" && c.Name != "_Import")
                {
                    c.MouseDown += OnMouseDown;
                    c.MouseMove += OnMouseMove;
                }
            }
            #endregion
            #region Настройка цветов
            _Server.DrawItem += OnDrawComboBoxItem;
            #endregion
            #region Выход
            _Exit.Click += (s, a) => { settings.Save(); Application.Exit(); };
            #endregion
        }
        #region Перемещение окна за любой участок (обработчики)
        int x = 0, y = 0;
        void OnMouseDown(object sender, MouseEventArgs e) { x = e.X; y = e.Y; }
        void OnMouseMove(object sender, MouseEventArgs e)
        {
            if (ActiveControl == null)
            {
                if (e.Button == MouseButtons.Left)
                    Location = new Point(Location.X + (e.X - x), Location.Y + (e.Y - y));
            }
        }
        #endregion
        #region Настройка цветов
        void OnDrawComboBoxItem(object sender, DrawItemEventArgs e)
        {
            if (e.Index < 0) return;

            ComboBox combo = sender as ComboBox;
            if ((e.State & DrawItemState.Selected) == DrawItemState.Selected)
                e.Graphics.FillRectangle(new SolidBrush(Color.FromArgb(120, 120, 120)), e.Bounds);
            else
                e.Graphics.FillRectangle(new SolidBrush(combo.BackColor), e.Bounds);

            e.Graphics.DrawString(combo.Items[e.Index].ToString(), e.Font,
                                  new SolidBrush(combo.ForeColor),
                                  new Point(e.Bounds.X, e.Bounds.Y));

        }
        #endregion

        private void MainForm_Load(object sender, EventArgs e)
        {
            _Server.Items.AddRange(sql.GetDBNames());
            settings.Load();



            Select();
        }

        private void _Import_Click(object sender, EventArgs e)
        {
            Log.Append("Проверка полей");
            if (_XmlPath.TextLength != 0 && _Server.Text.Length != 0 && _Log.TextLength != 0 && _Pass.TextLength != 0)
            {
                Log.Done(true);

                sql.Connect(_Server.Text, _Login.Text, _Pass.Text);
                xml.Parse(_XmlPath.Text);
                sql.Import(xml.Database);
                
                sql.Disconnect();
            }
            else
            {
                Log.Done(false, "Заполните все поля");
            }
        }

        #region Выбор xml файла
        private void _XmlPath_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "xml files (*.xml)|*.xml";

            if (dialog.ShowDialog() == DialogResult.OK)
                _XmlPath.Text = dialog.FileName;
        }
        #endregion
    }
}
